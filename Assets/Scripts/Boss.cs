﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour {

    public GameObject player;

    public float moveSpeed;
    public float speedRotate;

    private Vector3 lookAtPosition;

    public PlayerController myPlayer;

    public State stateC;

    public enum State
    {
        enemyState, heroState
    }


    void FixedUpdate()
    {
        float distancePlayer = Vector3.Distance(transform.position,player.transform.position);

        if (distancePlayer < 30f && myPlayer.bossReady && !myPlayer.gameOver && !myPlayer.win )
        {
            stateC = State.heroState;
        }
        else
        {
            stateC = State.enemyState;
        }

        switch (stateC)
        {
            case State.enemyState:
                enemyWay();
                break;

            case State.heroState:
                followPlayer();
                break;
        }
    }

    public void followPlayer()
    {
        lookAtPosition = player.transform.position;
        lookAtPosition.y = transform.position.y;
        transform.LookAt(lookAtPosition);
        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position), speedRotate * Time.deltaTime);
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
    }

    public void enemyWay()
    {

    }


}
