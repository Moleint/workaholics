﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public GameObject target;
    public float distance;
    public int cameraPos;

    // Update is called once per frame
    void Update()
    {
        switch (cameraPos)
        {
            case 1:
                transform.position = new Vector3(target.transform.position.x + 18f, target.transform.position.y + distance -2f, target.transform.position.z);
                break;
            case 2:
                transform.position = new Vector3(target.transform.position.x + 28f, target.transform.position.y + distance - 2f, target.transform.position.z);
                break;
            default:
                transform.position = new Vector3(target.transform.position.x + 28f, target.transform.position.y + distance - 2f, target.transform.position.z);
                break;
        }
    }
}
