﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public Text txtTime;
    public Text poinstCounter;
    public Text poinstWindow;
    public Text poinsOvertWindow;

    public GameObject pauseWindow;
    public GameObject gameOverWindow;
    public GameObject winWindow;


    private int i;
    private float hor;
    private float ver;

    public PlayerController myPlayer;

    public float timeValue;



    // Use this for initialization
    void Start()
    {
        pauseWindow.SetActive(false);
        gameOverWindow.SetActive(false);
        winWindow.SetActive(false);
        Time.timeScale = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (myPlayer.win != true)
        {
            timeValue -= Time.deltaTime;
        }

        if (timeValue <= 0f || myPlayer.gameOver == true)
        {
            timeValue = 0.0f;
            myPlayer.timeEnd();
            gameOver();
        }

        txtTime.text = "" + timeValue.ToString("F0");
        poinstCounter.text = "" + myPlayer.points.ToString("F0");

        if (myPlayer.win == true)
        {
            winWindow.SetActive(true);
            poinstWindow.text = "POINTS X " + myPlayer.points.ToString("F0");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseGame();
        }
    }

    public void pauseGame()
    {

        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        pauseWindow.SetActive(true);
    }

    public void gameOver()
    {

        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        gameOverWindow.SetActive(true);
        poinsOvertWindow.text = "POINTS X " + myPlayer.points.ToString("F0");
    }

    public void resumeGame()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            pauseWindow.SetActive(false);
        }

    }

    public void goHome()
    {
        resumeGame();
        SceneManager.LoadScene("Menu");
    }


    public void ExitGame()
    {
        Application.Quit();
    }


}