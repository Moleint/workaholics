﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy_shooter : MonoBehaviour {

    public Transform playerTarget;
    public Rigidbody m_EmailShell;
    public Transform m_FireTransform;
    public AudioSource m_ShootingAudio;
    public AudioClip m_FireClip;
    public float m_LauchForce = 10f;
    public float fireTime = 0.5f;
    public float distance = 15f;

    public int pooledCant = 8;
    List<GameObject> bullets;
    public GameObject bullet;

    private Rigidbody shellInstance;
    private Vector3 lookAtPosition;

    void Start()
    {
        bullets = new List<GameObject>();

        for (int i = 0; i < pooledCant; i++)
        {
            GameObject obj = (GameObject)Instantiate(bullet);
            obj.SetActive(false);
            bullets.Add(obj);
        }

        InvokeRepeating("Fire", fireTime, fireTime);
    }

	
	// Update is called once per frame
	void FixedUpdate () {
       
            lookAtPosition = playerTarget.position;
            lookAtPosition.y = transform.position.y;
            transform.LookAt(lookAtPosition);
	}


    void Fire()
    {
       


    /*Instantiate de emails*/
    float distancePlayer = Vector3.Distance(transform.position, playerTarget.transform.position);

        if (distancePlayer < distance)
        {
            for (int i = 0; i < bullets.Count; i++)
            {
                if (!bullets[i].activeInHierarchy)
                {
                    bullets[i].transform.position = m_FireTransform.position;
                    bullets[i].transform.rotation = Quaternion.identity;
                    bullets[i].SetActive(true);
                    shellInstance = bullets[i].GetComponent<Rigidbody>();
                    break;
                }
            }

            shellInstance.velocity = m_FireTransform.forward * m_LauchForce;
            /*Audio de email*/
            m_ShootingAudio.clip = m_FireClip;
            m_ShootingAudio.Play();
        }
    }
}
