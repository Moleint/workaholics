﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private CharacterController _playerController;
    private Rigidbody rb;
    private Animator animaPlayer;

    public bool isGrounded;
    public float direction = 1f;
    public float gravity;
    public float moveSpeed;
    private float fallSpeed = 1f;
    public float jumpSpeed;
    public Image damageImage;

    public bool damaged;

    public float xSpeed = 1f;

    public float _jump;
    private Vector3 _movePlayer = Vector3.zero;
    private Vector3 _turnPlayer;

    public GameObject[] hearts;
    private int health;

    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.5f);


    public CameraController cameraC;


    public int points;
    public bool win;
    private float timeValue;
    public bool gameOver;
    public bool bossReady = false;

    // Use this for initialization
    void Start()
    {
        win = false;
        points = 0;
        _playerController = GetComponent<CharacterController>();
        animaPlayer = GetComponentInChildren<Animator>();
        health = hearts.Length;
    }


    void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }

    // Update is called once per frame

    void FixedUpdate()
    {
        if (!gameOver)
        {
            IsGrounded();
            Fall();
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");
            Move(h, v);
            Animating(h, v);
        }
    }


    void Move(float h, float v)
    {

         _movePlayer = new Vector3(v * -1, 0, h);
        // _movePlayer *= moveSpeed;

         if (_movePlayer != Vector3.zero)
         transform.rotation = Quaternion.LookRotation(_movePlayer);
         //_playerController.Move(_movePlayer / 8);


        if (isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }     
        }
        else
        {
            Fall();
        }

        _playerController.Move(_movePlayer * moveSpeed * Time.deltaTime);

    }


    void Jump()
    {
        animaPlayer.SetBool("isGrounded", false);
       // animaPlayer.SetBool("isRunning", false);
        fallSpeed = -jumpSpeed;
    }

    void Fall()
    {
        if (!isGrounded)
        {
            fallSpeed += gravity * Time.deltaTime;
        }
        else {
            if (fallSpeed > 0)
            {
                fallSpeed = 0;
                animaPlayer.SetBool("isGrounded", true);
            }
        }
        _playerController.Move(new Vector3(0, -fallSpeed) * Time.deltaTime);
    }


    void IsGrounded()
    {
        isGrounded = (Physics.Raycast(transform.position, -transform.up, _playerController.height / 1.5f));
    }

    void Animating(float h, float v)
    {

        bool walk = !((v == 0) && (h == 0));
        animaPlayer.SetBool("isRunning", walk);

    }

    public void timeEnd()
    {
        gameOver = true;
    }


    void OnTriggerEnter(Collider obj)
    {

        if (obj.gameObject.tag == "Meta")
        {
            win = true;
        }

        if (obj.gameObject.tag == "Enemigos")
        {
            damage();
        }

        if (obj.gameObject.tag == "Coins")
        {
            points += 10;
            obj.GetComponent<AudioSource>().Play();
            Destroy(obj.gameObject, 0.2f);
        }

        if (obj.gameObject.tag == "changeCam")
        {
            cameraC.cameraPos = 1;
        }

        if (obj.gameObject.tag == "changeCam2")
        {
            cameraC.cameraPos = 2;
            bossReady = true;
        }

    }


    public void damage()
    {
        health--;
        hearts[health].SetActive(false);

        damaged = true;

        if (health == 0)
        {
            gameOver = true;
        }
    }

}
