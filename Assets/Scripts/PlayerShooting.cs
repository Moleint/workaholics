﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {

    public Rigidbody m_PaperShell;
    public Transform m_FireTransform;
    public AudioSource m_ShootingAudio;
    public AudioClip m_FireClip;
    public float m_LauchForce = 1f;

    private bool m_Fired;
    private Animator animaPlayer;


    void Start()
    {
        animaPlayer = GetComponentInChildren<Animator>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            m_Fired = false;
            animaPlayer.SetBool("isShooting", true);
        }
        else if (Input.GetKeyUp(KeyCode.K) && !m_Fired)
        {
            animaPlayer.SetBool("isShooting", false);
            Fire();
        }
    }

    void Fire()
    {
        m_Fired = true;

        Rigidbody shellInstance = Instantiate(m_PaperShell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        shellInstance.velocity = m_FireTransform.forward * m_LauchForce;



        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();
    }
	
}
