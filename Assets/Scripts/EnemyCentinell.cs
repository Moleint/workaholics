﻿using UnityEngine;
using System.Collections;

public class EnemyCentinell : MonoBehaviour {

    public GameObject player;

    public GameObject[] points;

    public float moveSpeed;
    public float rotationSpeed;
    private int randomPoint;

    private Vector3 lookAtPosition;
    private Vector3 lookAtWay;

    public PlayerController myPlayer;

    public State stateC;

    public float timeBetweenAttacks = 0.5f;
    float timer;
    private bool attack = true;

    public enum State
    {
        enemyState, heroState
    }

    void Start()
    {

        EnemyF();
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= timeBetweenAttacks && myPlayer.damaged == false && !myPlayer.gameOver)
        {
            attack = true;
        }

        if (myPlayer.damaged)
        {
            timer = 0f;
            attack = false;
        }
    }

    void FixedUpdate()
    {
        float distancePlayer = Vector3.Distance(transform.position, player.transform.position);

        if (distancePlayer < 8f && attack == true)
        {
            stateC = State.heroState;
        }
        else
        {
            stateC = State.enemyState;
        }

        switch (stateC)
        {
            case State.enemyState:
                enemyWay();
                break;

            case State.heroState:
                followPlayer();
                break;
        }
    }

    public void followPlayer()
    {
        lookAtPosition = player.transform.position;
        lookAtPosition.y = transform.position.y;
        transform.LookAt(lookAtPosition);
        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position), rotationSpeed * Time.deltaTime);
        transform.position += transform.forward * moveSpeed * Time.deltaTime;

    }

    public void enemyWay()
    {
        float distance = Vector3.Distance(transform.position, points[randomPoint].transform.position);
        lookAtWay = points[randomPoint].transform.position;
        lookAtWay.y = transform.position.y;
        transform.LookAt(lookAtWay);
        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(points[randomPoint].transform.position - transform.position), rotationSpeed * Time.deltaTime);
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
        Debug.DrawLine(transform.position, points[randomPoint].transform.position, Color.red);
        if (distance < 2f) EnemyF();
    }


    public void EnemyF()
    {
        randomPoint = Random.Range(0, points.Length);
    }


}
